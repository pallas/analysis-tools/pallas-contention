# Pallas Communication Matrix

We provide a program that proposes an analysis of the contention of a MPI program based on [SCI Metric](https://hal.science/hal-02179717v1).
Using the Pallas trace format, we analyse the program and generate a SCI metric score for each sequence of each thread.

## Building Pallas
You first need to have the pallas library installed.

```bash
git clone git@gitlab.inria.fr:pallas/pallas.git
cd pallas
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLATION_DIR
make && make install
```

This will build and install the Pallas library and a modified OTF2 library, as well as and  `pallas_info` and `pallas_print`,
which are tools made to read Pallas traces

If you want to enable SZ and ZFP, you should install them, and then add `-DSZ_ROOT_DIR=<your SZ installation>`
and `-DZFP_ROOT_DIR=<your ZFP installation>` to the cmake command line. Documentation is built automatically if Doxygen is installed.


## Building Pallas Communication matrix

You just need to compile using CMake:


```bash
cd pallas-contention
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLATION_DIR
make && make install
```
This will create the `pallas_contention` program.


## Usage

### Pallas
After compiling Pallas, install [ezTrace](https://eztrace.gitlab.io/eztrace).
Make sure to build it from source, and to use the Pallas OTF2 library, not the normal OTF2 library.
You can check `which otf2-config` to see if you have the correct one. If not, check your PATH and LD_LIBRARY_PATH variables.

Make sure to enable the relevant ezTrace modules (MPI for tracing MPI functions in the case of the communication matrix).
Then trace any program by running `mpirun -np N eztrace -t <your modules> <your program>`.
The trace file will be generated in the `<your program>_trace` folder.

### Pallas Contention
Then use `pallas_contention -t <your program>_trace/eztrace_log.pallas`to generate a file containing a CSI metric analysis per thread and per sequence.

This will generate a `pallas_contention.sci` file containing all this information in a human-readable and easy-to-parse format.

The full option list of the `pallas_communication_matrix` are as follows :
  * `-n OUTPUT_FILE`:   Name of the output file. Extension must be .mat. By default, the name of the file with be `pallas_comm_matrix.mat`
  * `-? -h`: Display help and full description of the program



## About

Pallas implements a subset of the [OTF2](https://www.vi-hps.org/projects/score-p) API.
It also implements the [Murmur3 hashing function](https://github.com/PeterScott/murmur3).

See the Pallas repository for full details.

## Contributing

Contribution to Pallas are welcome. Just send us a pull request.

## License
Pallas is distributed under the terms of both the BSD 3-Clause license.

See [LICENSE](LICENSE) for details
