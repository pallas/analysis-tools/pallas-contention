/*
 * Copyright (C) Telecom SudParis
 * See LICENSE in top-level directory.
 */
#include <algorithm>
#include <iostream>
#include <string>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <cassert>

#include "pallas/pallas.h"
#include "pallas/pallas_archive.h"
#include "pallas/pallas_read.h"
#include "pallas/pallas_storage.h"
#include "pallas/pallas_write.h"


using namespace pallas;


#define EPSILON 0

struct seq_info {
  int seq_id;
  uint64_t min_duration;
  uint64_t max_duration;
  uint64_t mean_duration;
  uint64_t sci;
  size_t nb_occurences;
};

double formatDuration(pallas_duration_t duration) {
  return duration * 1.0 / 1e9;
}

void sequence_duration_thread(Thread *thread, std::ofstream &output_stream, std::string &indent) {
  // compute thread duration = duration of the first sequence in Thread::sequences
  uint64_t thread_duration = thread->getDuration();

  int nb_sequence_to_print = 0;
  std::vector<struct seq_info> sequences;
  sequences.reserve(thread->nb_sequences);

  /* iterate over each sequence */
  for (size_t i = 1; i < thread->nb_sequences; i++) {
    /* Get current sequence */
    auto curr_seq = thread->sequences[i];
    //if (curr_seq->durations->size <= 2) continue;

    /* counter for SCI metric for the current sequence*/
    uint64_t sci_metric = (curr_seq->durations->mean - curr_seq->durations->min) * curr_seq->durations->size;

    struct seq_info s;
    s.seq_id = i;
    s.min_duration = curr_seq->durations->min;
    s.max_duration = curr_seq->durations->max;
    s.mean_duration = curr_seq->durations->mean;
    s.sci = sci_metric;
    s.nb_occurences = curr_seq->durations->size;

    sequences.push_back(s);
    if (s.sci > EPSILON) {
      nb_sequence_to_print++;
    }
  }

  if(! nb_sequence_to_print)
    return;

  std::sort (begin(sequences), end(sequences), [] (const struct seq_info &a, const struct seq_info &b) { return a.sci > b.sci; });

  output_stream << indent;
  output_stream << std::setw(15)<< "Sequence_id";
  output_stream << std::setw(15)<< "Min_duration";
  output_stream << std::setw(15)<< "Max_duration";
  output_stream << std::setw(15)<< "Mean_duration";
  output_stream << std::setw(15)<< "SCI";
  output_stream << std::setw(15)<< "Nb_occurence"<<std::endl;

  for(auto it = begin(sequences); it != end(sequences); it++) {
    /* Compute SCI metric of the sequence and write to output file */
    if ((*it).sci > EPSILON) {
      output_stream << indent << std::setw(15) <<(*it).seq_id;
      output_stream << std::setw(15)<< (*it).min_duration;
      output_stream << std::setw(15)<< (*it).max_duration;
      output_stream << std::setw(15)<< (*it).mean_duration;
      output_stream << std::setw(15)<< ((*it).sci * 1.0) / (thread_duration * 1.0);
      output_stream << std::setw(15)<< (*it).nb_occurences << std::endl;

    }
  }
}

void treat_contention(const GlobalArchive &trace, std::ofstream &output_stream) {
    std::string indent = "\t";
    for (uint aid = 0; aid < trace.nb_archives; aid++) {
        auto archive = trace.archive_list[aid];
        /* For each thread in that process */
        for (uint i = 0; i < archive->nb_threads; i++) {
            /* For each event in this thread */
            pallas::Thread *thread = archive->getThreadAt(i);
            if (thread == nullptr) continue;
	    if(thread->getEventCount() < 4) continue;
            output_stream << std::endl << "Information concerning Thread #" << i << " (id:" << thread->id << ", first_ts: "<< thread->getFirstTimestamp() <<", last_ts:"<< thread->getLastTimestamp() << ", duration: "<< formatDuration(thread->getDuration()) <<" s, nb_events: "<< thread->getEventCount()<<") :"
                          << std::endl;
            /* print sequence durations in the thread */
            sequence_duration_thread(thread, output_stream, indent);
            archive->freeThreadAt(i);
        }
    }
}


void usage(const std::string &prog_name) {
    std::cout << std::endl << "Usage: " << prog_name << " -t trace_file [OPTION] " << std::endl;
    std::cout << "\t-t FILE   --- Path to the Pallas trace file" << std::endl;
    std::cout
            << "\t[OPTION] -n OUTPUT_FILE   --- Name of the output file. Extension should be .csi. By default, the name of the file with be pallas_contention.csi"
            << std::endl;
    std::cout << "\t[OPTION] -? -h   --- Display this help and exit" << std::endl;
}


int main(int argc, char **argv) {
    int nb_opts = 0;
    std::string trace_name = "";
    std::string output_file = "pallas_contention.csi";

    /* Parse arguments */
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-t")) {
            nb_opts++;
            trace_name = std::string(argv[nb_opts + 1]);
            nb_opts++;
        }
        if (!strcmp(argv[i], "-n")) {
            nb_opts++;
            output_file = std::string(argv[nb_opts + 1]);
            nb_opts++;
        } else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "-?")) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        } else {
            /* Unknown parameter name. Stop parsing the parameter list and breaking. */
            break;
        }
    }

/* If no trace given, display usage and exit */
    if (trace_name == "") {
        usage(argv[0]);
        return EXIT_SUCCESS;
    }



    /* Starting processing of trace */
    std::cout << "[pallas-contention] Starting contention analysis with given Pallas trace..." << std::endl;
    auto start = std::chrono::high_resolution_clock::now();


    /* Reading archive */
    auto trace = GlobalArchive();
    pallasReadGlobalArchive(&trace, const_cast<char *>(trace_name.c_str()));


    /*  Open stream */
    std::ofstream output_stream;
    output_stream.open(const_cast<char *>(output_file.c_str()), std::ofstream::out | std::ofstream::trunc);


    /* Parse the different sequences and parse contention score  */
    treat_contention(trace, output_stream);

    /* Close stream */
    output_stream.close();

    /* Print program duration */
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << std::endl << "[pallas-contention] *** Elapsed time: " << std::setw(7) << duration.count()
              << " nanoseconds. ***" << std::endl;

    return EXIT_SUCCESS;
}

/* -*-
   mode: c;
   c-file-style: "k&r";
   c-basic-offset 2;
   tab-width 2 ;
   indent-tabs-mode nil
   -*- */
